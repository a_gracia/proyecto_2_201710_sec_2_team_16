package estructuras;

//ESTA ESTRUCTURA EST� BASADA EN LA TABLA DE HASH DE ALGOLIST ALGORITHMS AND DATA STRUCTURES, PUEDE SER ENCONTRADA EN LA WEB http://www.algolist.net/Data_structures/Hash_table/Simple_example
public class THash {
	
	private int size;
	public class Hashes {
	      private int key;
	      private int value;
	 
	      Hashes(int key, int value) {
	            this.key = key;
	            this.value = value;
	      }     
	 
	      public int getKey() {
	            return key;
	      }
	 
	      public int getValue() {
	            return value;
	      }
	}
	
    Hashes[] table;

    public THash(int tsize) {
    	  size=tsize;
          table = new Hashes[size];
          for (int i = 0; i < size; i++)
                table[i] = null;
    }

    public int get(int key) {
          int hash = (key % size);
          while (table[hash] != null && table[hash].getKey() != key)
                hash = (hash + 1) % size;
          if (table[hash] == null)
                return -1;
          else
                return table[hash].getValue();
    }

    public void put(int key, int value) {
          int hash = (key % size);
          while (table[hash] != null && table[hash].getKey() != key)
                hash = (hash + 1) % size;
          table[hash] = new Hashes(key, value);
    }

}
