package estructuras;

import java.util.NoSuchElementException;


public class ArbolRojoNegro <K extends Comparable<K>, V> {
	public class NodoArbol<K extends Comparable<K>, V>
	{
		private K key;
		private V value;
		private boolean red;
		private NodoArbol<K,V> left, right;
		private int size;
		
		public NodoArbol (K pKey, V pValue, boolean color, int pSize)
		{
			this.key = pKey;
			this.value = pValue;
			this.red = color;
			this.size = pSize;
		}
		
	}
	
	private NodoArbol<K,V> root;
	public ArbolRojoNegro()
	{
	}
	
	/**
	 * 
	 */
	public boolean isRed(NodoArbol<K,V> x)
	{
		if(x == null)
		{
			return false;
		}
		else return x.red;
	}
	
	/**
	 * 
	 */
	public int size(NodoArbol<K,V> x)
	{
		if(x == null)
		{
			return 0;
		}
		else
		{
			return x.size;
		}
	}
	
	public int size()
	{
		return size(root);
	}
		
	public boolean isEmpty()
	{
		return size() == 0;
	}
	
	public V get(K key) {
       if(key == null)
       {
    	   return null;
       }
       else
       {
    	   return get(root,key);
       }
	}

	private V get(NodoArbol<K,V> x, K key) {
		while (x != null) {
			int cmp = key.compareTo(x.key);
			if      (cmp < 0) x = x.left;
			else if (cmp > 0) x = x.right;
			else              return x.value;
		}
		return null;
	}
       
	/**
	 * 
	 */
	public boolean contains(K key)
	{
		return get(key) != null;
	}
	
	/**
	 * 
	 */
	public void put(K key, V val) {
        if (val == null) {
            delete(key);
            return;
        }

        root = put(root, key, val);
        root.red = false;
       }

    
    private NodoArbol<K,V> put(NodoArbol<K,V> h, K key, V val) { 
        if (h == null) return new NodoArbol<K,V>(key, val, true, 1);

        int cmp = key.compareTo(h.key);
        if      (cmp < 0) h.left  = put(h.left,  key, val); 
        else if (cmp > 0) h.right = put(h.right, key, val); 
        else              h.value = val;

        if (isRed(h.right) && !isRed(h.left))      h = rotateLeft(h);
        if (isRed(h.left)  &&  isRed(h.left.left)) h = rotateRight(h);
        if (isRed(h.left)  &&  isRed(h.right))     flipColors(h);
        h.size = size(h.left) + size(h.right) + 1;

        return h;
    }
    
    public void deleteMin() {
      

        // if both children of root are black, set root to red
        if (!isRed(root.left) && !isRed(root.right))
            root.red = true;

        root = deleteMin(root);
        if (!isEmpty()) root.red = false;
        // assert check();
    }

    // delete the key-value pair with the minimum key rooted at h
    private NodoArbol<K,V> deleteMin(NodoArbol<K,V> h) { 
        if (h.left == null)
            return null;

        if (!isRed(h.left) && !isRed(h.left.left))
            h = moveRedLeft(h);

        h.left = deleteMin(h.left);
        return balance(h);
    }


    /**
     * Removes the largest key and associated value from the symbol table.
     */
    public void deleteMax() {
                // if both children of root are black, set root to red
        if (!isRed(root.left) && !isRed(root.right))
            root.red = true;

        root = deleteMax(root);
        if (!isEmpty()) root.red = false;
    }

    
    private NodoArbol<K,V> deleteMax(NodoArbol<K,V> h) { 
        if (isRed(h.left))
            h = rotateRight(h);

        if (h.right == null)
            return null;

        if (!isRed(h.right) && !isRed(h.right.left))
            h = moveRedRight(h);

        h.right = deleteMax(h.right);

        return balance(h);
    }
    
    public void delete(K key) { 
        if (!contains(key)) return;

        // if both children of root are black, set root to red
        if (!isRed(root.left) && !isRed(root.right))
            root.red = true;

        root = delete(root, key);
        if (!isEmpty()) root.red = false;
    }

    private NodoArbol<K,V> delete(NodoArbol<K,V> h, K key) { 

        if (key.compareTo(h.key) < 0)  {
            if (!isRed(h.left) && !isRed(h.left.left))
                h = moveRedLeft(h);
            h.left = delete(h.left, key);
        }
        else {
            if (isRed(h.left))
                h = rotateRight(h);
            if (key.compareTo(h.key) == 0 && (h.right == null))
                return null;
            if (!isRed(h.right) && !isRed(h.right.left))
                h = moveRedRight(h);
            if (key.compareTo(h.key) == 0) {
                NodoArbol<K,V> x = min(h.right);
                h.key = x.key;
                h.value = x.value;
                h.right = deleteMin(h.right);
            }
            else h.right = delete(h.right, key);
        }
        return balance(h);
    }
    
 // make a left-leaning link lean to the right
    private NodoArbol<K,V> rotateRight(NodoArbol<K,V> h) {
        // assert (h != null) && isRed(h.left);
        NodoArbol<K,V> x = h.left;
        h.left = x.right;
        x.right = h;
        x.red = x.right.red;
        x.right.red = true;
        x.size = h.size;
        h.size = size(h.left) + size(h.right) + 1;
        return x;
    }

    // make a right-leaning link lean to the left
    private NodoArbol<K,V> rotateLeft(NodoArbol<K,V> h) {
        // assert (h != null) && isRed(h.right);
    	NodoArbol<K,V> x = h.right;
        h.right = x.left;
        x.left = h;
        x.red = x.left.red;
        x.left.red = true;
        x.size = h.size;
        h.size = size(h.left) + size(h.right) + 1;
        return x;
    }

    // flip the colors of a node and its two children
    private void flipColors(NodoArbol<K,V> h) {
        // h must have opposite color of its two children
        // assert (h != null) && (h.left != null) && (h.right != null);
        // assert (!isRed(h) &&  isRed(h.left) &&  isRed(h.right))
        //    || (isRed(h)  && !isRed(h.left) && !isRed(h.right));
        h.red = !h.red;
        h.left.red = !h.left.red;
        h.right.red = !h.right.red;
    }

    // Assuming that h is red and both h.left and h.left.left
    // are black, make h.left or one of its children red.
    private NodoArbol<K,V> moveRedLeft(NodoArbol<K,V> h) {
        // assert (h != null);
        // assert isRed(h) && !isRed(h.left) && !isRed(h.left.left);

        flipColors(h);
        if (isRed(h.right.left)) { 
            h.right = rotateRight(h.right);
            h = rotateLeft(h);
            flipColors(h);
        }
        return h;
    }

    // Assuming that h is red and both h.right and h.right.left
    // are black, make h.right or one of its children red.
    private NodoArbol<K,V> moveRedRight(NodoArbol<K,V> h) {
        // assert (h != null);
        // assert isRed(h) && !isRed(h.right) && !isRed(h.right.left);
        flipColors(h);
        if (isRed(h.left.left)) { 
            h = rotateRight(h);
            flipColors(h);
        }
        return h;
    }

    // restore red-black tree invariant
    private NodoArbol<K,V> balance(NodoArbol<K,V> h) {
        // assert (h != null);

        if (isRed(h.right))                      h = rotateLeft(h);
        if (isRed(h.left) && isRed(h.left.left)) h = rotateRight(h);
        if (isRed(h.left) && isRed(h.right))     flipColors(h);

        h.size = size(h.left) + size(h.right) + 1;
        return h;
    }
       
    public int height() {
        return height(root);
    }
    private int height(NodoArbol<K,V> x) {
        if (x == null) return -1;
        return 1 + Math.max(height(x.left), height(x.right));
    }
    
    public K min() {
        if (isEmpty()) throw new NoSuchElementException("called min() with empty symbol table");
        return min(root).key;
    } 

    // the smallest key in subtree rooted at x; null if no such key
    private NodoArbol<K,V> min(NodoArbol<K,V> x) { 
        // assert x != null;
        if (x.left == null) return x; 
        else                return min(x.left); 
    } 

    /**
     * Returns the largest key in the symbol table.
     * @return the largest key in the symbol table
     * @throws NoSuchElementException if the symbol table is empty
     */
    public K max() {
        if (isEmpty()) throw new NoSuchElementException("called max() with empty symbol table");
        return max(root).key;
    } 

    // the largest key in the subtree rooted at x; null if no such key
    private NodoArbol<K,V> max(NodoArbol<K,V> x) { 
        // assert x != null;
        if (x.right == null) return x; 
        else                 return max(x.right); 
    } 
}