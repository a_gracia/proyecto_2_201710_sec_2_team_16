package estructuras;

import java.util.Iterator;

import javax.print.attribute.standard.Sides;
import javax.swing.plaf.metal.MetalBorders.ToggleButtonBorder;

public class ListaEncadenada<T> implements Iterable<T>, ILista<T>{

	private NodoSencillo<T> primera = null;
	private NodoSencillo<T> actualactual = primera;
	private NodoSencillo<T> ultimo = primera;

	int listSize = darNumeroElementos();
	public Iterator<T> iterator() {
		Iterator<T> it = new MiIterator<T>(primera);
		return it;
	}
	public class MiIterator <T> implements Iterator <T>  {

		NodoSencillo<T> actual;

		public MiIterator (NodoSencillo<T> PprimerElemento){
			actual = PprimerElemento; 
		}

		@Override
		public boolean hasNext() {

			if(darNumeroElementos() == 0){
				return false;
			}

			if(actual == null)
				return true;
			return actual.siguiente != null;
		}

		@Override
		public T next() {
			if(actual == null)
				actual = (NodoSencillo<T>) primera;
			else{
				actual = actual.siguiente;
			}
			return actual.elemento;
		}

	}



	public void agregarElementoFinal(T elem) {

		NodoSencillo<T> agregar = new NodoSencillo<T>(elem);
		actualactual = primera;
		if(primera == null){
			primera = agregar;
			ultimo = primera;
		}else{
		while(actualactual != null){
			if(actualactual.siguiente==null){
				actualactual.siguiente = agregar;
				break;
			}
			else
				actualactual = actualactual.siguiente;
		}}
	}

	public NodoSencillo<T> darElemento(int pos) {

		NodoSencillo<T> temp = primera;
		int cont = 0;
		while(temp.siguiente != null && cont <= pos-1 )
		{
			temp = temp.siguiente;
			cont++;
		}

		return temp;
	}

	public T darElementoT(int pos) {

		NodoSencillo<T> actual = primera;
		for(;pos > 0 && actual != null; pos--)
			actual = actual.siguiente;
		return actual == null ? null : actual.elemento; 
	}
	
	public T darElementoFinal()
	{
		
		return darElementoT(darNumeroElementos()-1);
	}


	public int darNumeroElementos() {
		// TODO Auto-generated method stub
		NodoSencillo<T> actual = primera;
		int pos = 0;
		while(actual!=null && actual.elemento != null)
		{
			pos++;
			actual=actual.siguiente;
		}
		return pos;

	}



	/**
	 * Elimina dada una posicion
	 */
	public T remove(int posicion){

		if(primera == null)
			throw new IndexOutOfBoundsException();

		else{
			NodoSencillo<T> actual = primera;
			NodoSencillo<T> anterior = null;

			if(posicion == 0){
				primera = actual.siguiente;
				return primera.elemento;
			}

			else
			{
				while(actual != null  && posicion > 0)
				{
					anterior = actual;
					actual = actual.siguiente;
					posicion --;					
				}

				if(posicion > 0)
					throw new IndexOutOfBoundsException();

				else
				{
					if(actual != null){
						anterior.siguiente = actual.siguiente;//Revisar en caso de error
						return actual.elemento;
					}


					else
						anterior.siguiente = null;
					return anterior.elemento;


				}	
			}
		}

	}



	public static class NodoSencillo <T>
	{
		T elemento;
		NodoSencillo<T> siguiente;


		public NodoSencillo(T elemento) {
			this.elemento = elemento;
		}

		@Override
		public String toString() {
			// TODO Auto-generated method stub
			return elemento.toString();
		}
	}

}
