package estructuras;

import java.util.Iterator;

public class EncadenamientoSeparadoTH<K extends Comparable<K> ,V> {
	
	private ListaLlaveValorSecuencial<K,V>[] hashtable;
	private int tot;//numero maximo de espacios en el hash table
	private int used;//numero de casillas en la hashtable ocupadas
	private Iterator<K> iter;
	
	public EncadenamientoSeparadoTH(int Pm)
	{
		hashtable = new ListaLlaveValorSecuencial[Pm];//no me deja crear arreglos genericos
		tot = Pm;
		iter = new Iterator<K>()
		{

			@Override
			public boolean hasNext() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public K next() {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public void remove() {
				// TODO Auto-generated method stub
				
			}
			
		};
		for(int i = 0; i < hashtable.length; i++)
		{
			hashtable[i] = new ListaLlaveValorSecuencial<K, V>();
		}
	}
	
	public int getSize()
	{
		int counter = 0;
		for(int i = 0; i < hashtable.length; i++)
		{
			if(hashtable[i] != null)
			{
				counter++;
			}
		}
		return counter;
	}
	
	public boolean isEmpty()
	{
		if(getSize() == 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public boolean hasKey(K key )
	{
		int pos = hash(key);
		return hashtable[pos].hasKey(key);
//		boolean found = false;
//		for(int i = 0; i < hashtable.length && !found; i++)
//		{
//			if(hashtable[i].hasKey(key))
//			{
//				found = true;
//			}
//		}
//		return found;
	}
	
	public V getData(K key)
	{
		V resp = null;
//		for(int i = 0; i < hashtable.length; i++)
//		{
//			if(hashtable[i].hasKey(key))
//			{
//				resp = hashtable[i].getData(key);
//				break;
//			}
//		}
		if(hasKey(key))
		{
			int pos = hash(key);
			resp = hashtable[pos].getData(key);
		}
		return resp;
	}
	
	public void insert(K pKey, V pData)
	{
		// rehashing
		if(checkTable() <= 2)
		{
			ListaLlaveValorSecuencial<K,V>[] lista = new ListaLlaveValorSecuencial[tot/2];
			tot = tot/2;
			for(int i = 0; i < hashtable.length; i++)
			{
				Iterator iter = hashtable[i].llaves();
				while(iter.hasNext())
				{
					K sig = (K)iter.next();
					V data = hashtable[i].getData(sig);
					int pos1 = hash(sig); //no estoy haciendo este paso bien, donde estoy metiendo de vuelta los valores?
					lista[pos1].Insert(sig, data);
				}
			}
			hashtable = lista;
		}
		else if(checkTable() >= 8)
		{
			ListaLlaveValorSecuencial<K,V>[] lista2 = new ListaLlaveValorSecuencial[2*tot];
			tot = tot*2;
			for(int i = 0; i < hashtable.length; i++)
			{
				Iterator iter = hashtable[i].llaves();
				while(iter.hasNext())
				{
					K sig = (K)iter.next();
					V data = hashtable[i].getData(sig);
					int pos1 = hash(sig); //no estoy haciendo este paso bien, donde estoy metiendo de vuelta los valores?
					lista2[pos1].Insert(sig, data);
				}
			}
			hashtable = lista2;
		}
		
		int pos = hash(pKey);
		hashtable[pos].Insert(pKey, pData);
		used++;
	}
	
	public Iterator<K> llaves ()
	{
		return iter;
	}
	
	public int hash(K llave)
	{
		// usa modulo! cada clase tiene un hashcode, usa el valor abosulot de eso %tot para obetener un valor entre 
		//0 y m-1! el hash cambia al hacer resizing porque tot cambia!
		//
		return Math.abs(llave.hashCode()) % tot;
	}
	
	public int[] darLongitudListas()
	{
		int[] resp = new int[hashtable.length];
		for(int i = 0; i < hashtable.length; i++)
		{
			resp[i] = hashtable[i].darSize();
		}
		return resp;
	}
	
	public double checkTable()
	{
		return used/tot;
	}
	
	
	
}
