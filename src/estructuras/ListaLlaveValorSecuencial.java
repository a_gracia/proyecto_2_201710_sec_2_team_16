package estructuras;

import java.util.Iterator;

public class ListaLlaveValorSecuencial<K extends Comparable<K>, V> {
		public class NodoLista<K extends Comparable<K>, V> //que quiere decir hiding?
		{
			private NodoLista<K,V> next;
			private V data;
			private K key;
			
			public NodoLista (V dato, K pKey)
			{
				data = dato;
				key =pKey;
			}
			
			public void setNext(NodoLista<K,V> t)
			{
				this.next = t;
			}
			
			public void setData (V dato)
			{
				this.data = dato;
			}
			
			public V getData()
			{
				return this.data;
			}
			public NodoLista<K,V> getNext()
			{
				return this.next;
			}
			public K getKey()
			{
				return this.key;
			}
			public void setKey(K niuK)
			{
				this.key = niuK;
			}

		}
		
		private NodoLista<K,V> first;
		private int size = 0;
		
		public ListaLlaveValorSecuencial ()
		{
			first = null;
					
		}
		
		public int darSize()
		{
			return size;
		}
		
		public boolean isEmpty()
		{
			if(size == 0)
			{
				return true;
			}
			
			return false;
		}
		
		public boolean hasKey(K llave)
		{
			boolean found = false;
			NodoLista<K,V> temp = first;
			System.out.println(temp.getNext() != null);
			while(temp.getNext() != null && !found)
			{
				if(temp.getKey() == llave)
				{
					found = true;
				}
				temp = temp.getNext();
			}
			return found;
		}
		
		public V getData(K llave)
		{
			boolean found = false;
			V resp = null;
			NodoLista<K,V> temp = first;
			while(temp.getNext() != null && !found)
			{
				if(temp.getKey() == llave)
				{
					found = true;
					resp = temp.getData();
				}
				temp = temp.getNext();
			}
			return resp;
		}
		
		public void Insert(K key, V data)
		{
			if(first == null && data != null)
			{
				first = new NodoLista<K,V>(data,key);
			}
			else
			{
				//el dato es null(elimina)
				if(data == null)
				{
					NodoLista<K,V> temp = first;
					boolean found = false;
					while(temp.getNext() != null && !found)
					{
						if(temp.getNext().getKey() == key)
						{
							found = true;
						}
						temp = temp.getNext();
					}
					temp.setNext(null);
					size--;
				}
				else //el dato no es null, tenemos que ver si ya existe la llave o no
				{
					boolean found = false;
					NodoLista<K,V> temp = first;
					while(temp.getNext() != null && !found)
					{
						if(temp.getKey() == key)
						{
							found = true;
						}
						temp = temp.getNext();
					}
					if(found == true)//modifica
					{
						temp.setData(data);
					}
					else//agrega
					{
						temp.setNext(new NodoLista<K,V>(data, key));
						size++;
						System.out.println(data);
					}
				}
			}
		}
		
		public Iterator<K> llaves ()
		{
			return  new Iterator<K> ()
			{
			NodoLista<K,V> temp = first;
			
				@Override
				public boolean hasNext() {
					if(temp != null)
					{
						if(temp.getNext() != null)
						{
							return true;
						}
						else
						{
							return false;
						}
					}
					else
					{
						return false;
					}
				}

				@Override
				public K next() {
					temp = temp.getNext();
					return temp.getKey();
				}

				@Override
				public void remove() {
					// TODO Auto-generated method stub
					
				}
		
			};
		}

}
