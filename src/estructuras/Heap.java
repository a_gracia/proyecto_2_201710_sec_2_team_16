package estructuras;

import java.util.ArrayList;

public class Heap<T extends Comparable> {

	private ArrayList<T> list;
	
	public Heap()
	{
		list = new ArrayList<T>();
	}
	
	public void swim(int pos)
	{
		T padre = list.get(Math.floorDiv(pos, 2));
		T hijo = list.get(pos);
		if(hijo.compareTo(padre) > 0 && pos >= 1)
		{
			list.remove(pos);
			list.remove(Math.floorDiv(pos, 2));
			list.add(Math.floorDiv(pos, 2), hijo);
			list.add(pos, padre);
			swim(Math.floorDiv(pos, 2));
		}
	}
	
	public void sink(int pos)
	{
		T padre = list.get(pos);
//		if(pos == 0)
//		{
//			if(list.get(1).compareTo(padre) > 0 )
//			{
//				T hijo1 = list.get(1);
//				list.set(pos,hijo1);
//				list.set(1, padre);
//				sink(1);
//			}
//			else if(list.get(2).compareTo(padre) > 0)
//			{
//				T hijo2 = list.get(2);
//				list.set(pos,hijo2);
//				list.set(2, padre);
//				sink(2);
//			}
//		}
		if( (2*pos)+1 <= list.size()-1 && (2*pos)+2 <= list.size()-1 )
		{
			if(list.get((2*pos)+1).compareTo(padre) > 0 )
			{
				T hijo1 = list.get((2*pos)+1);
				list.set(pos,hijo1);
				list.set((2*pos)+1, padre);
				sink((2*pos)+1);
			}
			else if(list.get((2*pos)+2).compareTo(padre) > 0)
			{
				T hijo2 = list.get((2*pos)+2);
				list.set(pos,hijo2);
				list.set((2*pos)+2, padre);
				sink((2*pos)+2);
			}
		}
	}
	
	public void insert(T elem)
	{
		list.add(elem);
		swim(list.size()-1);
	}
	
	public T get()
	{
		T mayor = list.get(0);
		list.set(0, list.get(list.size()-1));
		list.remove(list.size()-1);
		sink(0);
		return mayor;
	}
	
	public boolean checkHeap()
	{
		for(int i = list.size()-1;i >= 0; i--)
		{
			T padre = list.get(i);
			if( (2*i)+1 <= list.size()-1 && (2*i)+2 <= list.size()-1 )
			{
				if(list.get((2*i)+1).compareTo(padre) > 0)
				{
					return false;
				}
				else if(list.get((2*i)+2).compareTo(padre) > 0)
				{
					return false;
				}
			}
		}
		return true;
	}
}
