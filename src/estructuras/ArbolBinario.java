package estructuras;

public class ArbolBinario<T extends Comparable<T>> {
	
	public class NodoBinario<T extends Comparable<T>>
	{
		private NodoBinario<T> left;
		private NodoBinario<T> right;
		private T data;
		public NodoBinario<T> getLeft() {
			return left;
		}
		public void setLeft(NodoBinario<T> left) {
			this.left = left;
		}
		public NodoBinario<T> getRight() {
			return right;
		}
		public void setRight(NodoBinario<T> right) {
			this.right = right;
		}
		public T getData() {
			return data;
		}
		public void setData(T data) {
			this.data = data;
		}
		
		public NodoBinario (T elem)
		{
			data = elem;
		}
		
		public NodoBinario<T> search( T elem)
		{
			int cmp = this.getData().compareTo(elem);
			if( cmp == 0)
			{
				return this;
			}
			else if( cmp > 0)
			{
				return this.getLeft().search(elem);
			}
			else
			{
				return this.getRight().search(elem);
			}
		}
		
		public boolean check()
		{
			if(this.notLeaf())
			{
				if(this.getData().compareTo(this.getLeft().getData()) < 0 || this.getData().compareTo(this.getRight().getData() ) > 0)
				{
					return false;
				}
				return this.getLeft().check() || this.getRight().check();
			}
			return true;
		}
		
		public boolean notLeaf()
		{
			return this.getLeft() != null && this.getRight() != null;
		}
	}
	
	private NodoBinario<T> root;
	
	public ArbolBinario ()
	{
		root = null;
	}
	
	
	public NodoBinario<T> insert(NodoBinario<T> x, T elem)
	{
		if( x == null)
		{
			return new NodoBinario<T>(elem);
		}
		int cmp = elem.compareTo(x.getData());
		if(cmp < 0)
		{
			x.setLeft( insert(x.getLeft(), elem));
		}
		else if( cmp > 0)
		{
			x.setRight(insert(x.getRight(), elem));
		}
		return x;
	}
	
	public void insert(T elem)
	{
		root = insert(root,elem);
	}
	
	public NodoBinario<T> search (T elem)
	{
		if(root == null)
		{
			return null;
		}
		return root.search(elem);
	}
	
	public boolean chechBST()
	{
		return root.check();
	}

}
