package vos;

import java.util.Date;

import estructuras.ILista;
import estructuras.ListaEncadenada;

public class VOPelicula implements Comparable<VOPelicula> {
	/*
	 * nombre de la pel�cula
	 */
	private String nombre;
	
	/*
	 * Fecha de lanzamiento de la pel�cula
	 */
	private Date fechaLanzamineto;
	
	/*
	 * Lista con los generos asociados a la pel�cula
	 */
	
	private ListaEncadenada<VOGeneroPelicula> generosAsociados;
	
	/*
	 * votos totales sobre la pel�cula
	 */
	 private int votostotales;  
	 
	 /*
	  * promedio anual de votos (hasta el 2016)
	  */
	 private double promedioAnualVotos; 
	 
	 /*
	  * promedio IMBD
	  */
	 
	 private double ratingIMBD;	
	 
	 /**
	  * 
	  */
	 private int id;
	 
	 public VOPelicula() {
		// TODO Auto-generated constructor stub
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Date getFechaLanzamineto() {
		return fechaLanzamineto;
	}

	public void setFechaLanzamineto(Date fechaLanzamineto) {
		this.fechaLanzamineto = fechaLanzamineto;
	}

	public ListaEncadenada<VOGeneroPelicula> getGenerosAsociados() {
		return generosAsociados;
	}

	public void setGenerosAsociados(ListaEncadenada<VOGeneroPelicula> gen) {
		this.generosAsociados = gen;
	}

	public int getVotostotales() {
		return votostotales;
	}

	public void setVotostotales(int votostotales) {
		this.votostotales = votostotales;
	}

	public double getPromedioAnualVotos() {
		return promedioAnualVotos;
	}

	public void setPromedioAnualVotos(double promedioAnualVotos) {
		this.promedioAnualVotos = promedioAnualVotos;
	}

	public double getRatingIMBD() {
		return ratingIMBD;
	}

	public void setRatingIMBD(double ratingIMBD) {
		this.ratingIMBD = ratingIMBD;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public int compareTo(VOPelicula o) {
		return fechaLanzamineto.compareTo(o.fechaLanzamineto);
	}
	 
	 
	
}
