package vos;




import estructuras.ILista;
import estructuras.ListaEncadenada;

public class VOGenero {
	
	private VOGeneroPelicula genero;
	
	private ListaEncadenada<VOPelicula> peliculas= new ListaEncadenada<VOPelicula>();

	public VOGeneroPelicula getGenero() {
		return genero;
	}

	public void setGenero(VOGeneroPelicula voGeneroPelicula) {
		this.genero = voGeneroPelicula;
	}

	public ListaEncadenada<VOPelicula> getPeliculas() {
		return peliculas;
	}

	public void setPeliculas(ListaEncadenada<VOPelicula> peliculas) {
		this.peliculas = peliculas;
	} 
	public void addP(VOPelicula e){
		peliculas.agregarElementoFinal(e);
	}

}

