package vos;

import estructuras.ILista;
import estructuras.ListaEncadenada;

/**
 * @author Venegas
 *
 */

public class VOUsuarioPelicula {
	
	/**
	 * nombre de la pel�cula
	 */

	private String nombrepelicula;
	
	/**
	 * rating dado por el usuario
	 */
	private double ratingUsuario;
	
	/**
	 * rating calculado por el sistema
	 */

	private double ratingSistema;
	
	/**
	 * error sobre el rating
	 */
	
	private double errorRating;
	
	/**
	 * list de tags generados por el usuario sobre la pelicula
	 */
	
	private ListaEncadenada<VOTag> tags;
	
	/**
	 * id del usuario
	 */
	
	private Integer idUsuario;
	
	private Long timestamp;
	
	public VOUsuarioPelicula() {
		// TODO Auto-generated constructor stub
	}
	
	public String getNombrepelicula() {
		return nombrepelicula;
	}

	public void setNombrepelicula(String nombrepelicula) {
		this.nombrepelicula = nombrepelicula;
	}

	public double getRatingUsuario() {
		return ratingUsuario;
	}

	public void setRatingUsuario(double ratingUsuario) {
		this.ratingUsuario = ratingUsuario;
	}

	public double getRatingSistema() {
		return ratingSistema;
	}

	public void setRatingSistema(double ratingSistema) {
		this.ratingSistema = ratingSistema;
	}

	public double getErrorRating() {
		return errorRating;
	}

	public void setErrorRating(double errorRating) {
		this.errorRating = errorRating;
	}

	public ListaEncadenada<VOTag> getTags() {
		return tags;
	}

	public void setTags(ListaEncadenada<VOTag> listaEncadenada) {
		this.tags = listaEncadenada;
	}

	public Integer getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}
}
