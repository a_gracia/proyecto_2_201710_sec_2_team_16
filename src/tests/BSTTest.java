package tests;

import estructuras.ArbolBinario;
import junit.framework.TestCase;

public class BSTTest extends TestCase {

	private ArbolBinario<Integer> bst;
	
	private void setupEscenario1()
	{
		bst = new ArbolBinario<Integer>();
		bst.insert(10);
		bst.insert(5);
		bst.insert(15);
		bst.insert(4);
		bst.insert(20);
		bst.insert(1);
		bst.insert(50);
	}
	
	public void  testInsert()
	{
		setupEscenario1();
		bst.insert(14);
		assertTrue("dejo de ser bst", bst.chechBST());
	}
}
