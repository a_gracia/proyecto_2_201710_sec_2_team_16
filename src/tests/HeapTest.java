package tests;

import estructuras.Heap;
import junit.framework.TestCase;

public class HeapTest extends TestCase {

	private Heap<Integer> heap;
	
	public void setUpEscenario1()
	{
		heap = new Heap<Integer>();
		heap.insert(548);
		heap.insert(540);
		heap.insert(469);
		heap.insert(320);
		heap.insert(230);
		heap.insert(340);
		heap.insert(270);
	}
	
	public void testInsertar()
	{
		//verificar que siga siendo heap
		setUpEscenario1();
		heap.insert(1000);
		assertTrue("dejo de ser heap!" ,heap.checkHeap());
	}
	
	public void testSacarMayor()
	{
		setUpEscenario1();
		int mayor = heap.get();
		assertEquals(548, mayor);
		assertTrue("dejo de ser heap!", heap.checkHeap());
	}
}
