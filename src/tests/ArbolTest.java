package tests;

import estructuras.ArbolRojoNegro;
import junit.framework.TestCase;

public class ArbolTest extends TestCase {
	
	private static ArbolRojoNegro<Integer,String> rbt = new ArbolRojoNegro<Integer, String>();
	
	private void setupEscenario()
	{
		rbt.put(1, "a");
		rbt.put(2, "b");
		rbt.put(3, "c");
		rbt.put(4, "d");
		rbt.put(5, "e");
		rbt.put(6, "f");
		rbt.put(7, "g");
		rbt.put(8, "h");
		rbt.put(9, "i");
	}
	
	/**
	 * 
	 */
	public void testSize()
	{
		setupEscenario();
		assertEquals(9, rbt.size());
	}
	
	/**
	 * 
	 */
	public void testIsEmpty()
	{
		setupEscenario();
		assertFalse(rbt.isEmpty());
	}
	
	/**
	 * 
	 */
	public void testGet()
	{
		setupEscenario();
		String result = rbt.get(4);
		assertEquals("d", result);
	}
	
	/**
	 * 
	 */
	public void testContains()
	{
		setupEscenario();
		assertTrue(rbt.contains(2));
		assertFalse(rbt.contains(10));
	}
	
	/**
	 * 
	 */
	public void testPut()
	{
		setupEscenario();
		//ver que reemplaze
		rbt.put(8, "f");
		String result = rbt.get(8);
		assertEquals("f", result);
	}
	
	/**
	 * 
	 */
	public void testDeleteMin()
	{
		setupEscenario();
		rbt.deleteMin();
		assertFalse(rbt.contains(1));
	}
	
	/**
	 * 
	 */
	public void testDeleteMax()
	{
		setupEscenario();
		rbt.deleteMax();
		assertFalse(rbt.contains(9));
	}
	
	/**
	 * 
	 */
	public void testDelete()
	{
		setupEscenario();
		rbt.delete(5);
		assertFalse(rbt.contains(5));
	}
	
	/**
	 * 
	 */
	public void testMin()
	{
		  setupEscenario();
		  int result = rbt.min();
		  assertEquals(1, result);
	}
	
	/**
	 * 
	 */
	public void TestMax()
	{
		setupEscenario();
		int result = rbt.max();
		assertEquals(9, result);
	}

}
