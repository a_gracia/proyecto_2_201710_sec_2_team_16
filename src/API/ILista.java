package API;

import estructuras.ListaEncadenada.NodoSencillo;

public interface ILista<T> extends Iterable<T>{
	
	void agregarElementoFinal(T elem);
	
	NodoSencillo<T> darElemento(int pos);
	
	T darElementoT(int pos);

	T darElementoFinal();
	
	int darNumeroElementos();
	
	T remove(int posicion);
}
