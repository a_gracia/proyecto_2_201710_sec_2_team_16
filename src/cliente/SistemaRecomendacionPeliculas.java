package cliente;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.csvreader.CsvReader;

import API.ISistemaRecomendacionPeliculas;
import cliente.ClienteReq.Recomendacion;
import cliente.ClienteReq.Solicitud;
import estructuras.ArbolBinario;
import estructuras.ArbolRojoNegro;
import estructuras.EncadenamientoSeparadoTH;
import estructuras.Heap;
import estructuras.ILista;
import estructuras.ListaEncadenada;
import vos.VOGenero;
import vos.VOGeneroPelicula;
import vos.VOPelicula;
import vos.VOReporteSegmento;
import vos.VOTag;
import vos.VOUsuarioPelicula;

public class SistemaRecomendacionPeliculas implements ISistemaRecomendacionPeliculas{

	private ClienteReq cr;
	
	private Heap<Solicitud> heap; 
	private ArbolRojoNegro rbt;
	
	/**
	 * 
	 */
	private ListaEncadenada<VOPelicula> misPeliculas;
	
//	/**
//	 * 
//	 */
//	private ListaEncadenada<VORating> ratings;
	
	/**
	 * 
	 */
	private ListaEncadenada<VOTag> tags;
	
	/**
	 * 
	 */
	private ListaEncadenada<VOGenero> gposibles;
	
	/**
	 * 
	 */
	private ListaEncadenada<VOUsuarioPelicula> ratingspelis;
	
	public class Solicitud implements Comparable<Solicitud>
	{
		private Date primeraRevision;
		public Date getPrimeraRevision() {
			return primeraRevision;
		}



		private int idusuario; //aqui guardo el id de quein hizo la solicitud
		public int getIdusuario() {
			return idusuario;
		}



		private boolean esporid;
		public boolean isEsporid() {
			return esporid;
		}



		private int numeroratings;
		public int getNumeroratings() {
			return numeroratings;
		}



		private int numerotags;
		public int getNumerotags() {
			return numerotags;
		}



		private ListaEncadenada<VOUsuarioPelicula> aux;
		
		
		
		public void setPrimeraRevision(Date primeraRevision) {
			this.primeraRevision = primeraRevision;
		}



		public void setIdusuario(int idusuario) {
			this.idusuario = idusuario;
		}



		public void setEsporid(boolean esporid) {
			this.esporid = esporid;
		}



		public void setNumeroratings(int numeroratings) {
			this.numeroratings = numeroratings;
		}



		public void setNumerotags(int numerotags) {
			this.numerotags = numerotags;
		}



		@Override
		public int compareTo(Solicitud o) {
			if(this.esporid == false && o.esporid != false )
			{
				return -1;
			}
			else if(o.esporid == false && this.esporid != false)
			{
				return 1;
			}
			else
			{
				if(this.primeraRevision.compareTo(o.primeraRevision) != 0)
				{
					return this.primeraRevision.compareTo(o.primeraRevision);
				}
				else
				{
					if(this.numeroratings > o.numeroratings)
					{
						return 1;
					}
					else if(this.numeroratings < o.numeroratings)
					{
						return -1;
					}
					else
					{
						if(this.numerotags > o.numerotags)
						{
							return 1;
						}
						else if(this.numerotags < o.numerotags)
						{
							return -1;
						}
					}
				}
			}
		return (Integer) null; //esto querria decir que no todos los casos se estan considerando
	}
		



		public ListaEncadenada<VOUsuarioPelicula> getAux() {
			return aux;
		}



		public void setAux(ListaEncadenada<VOUsuarioPelicula> aux) {
			this.aux = aux;
		}
	}
	
	public class Recomendacion implements Comparable<Recomendacion>
	{
		private VOPelicula pelicula;
		private double prediccion;
		private String usuarioId;
		
		
		
		public String getUsuarioId() {
			return usuarioId;
		}



		public void setUsuarioId(String string) {
			this.usuarioId = string;
		}



		public VOPelicula getNombre() {
			return pelicula;
		}



		public void setNombre(VOPelicula nombre) {
			this.pelicula = nombre;
		}



		public double getPrediccion() {
			return prediccion;
		}



		public void setPrediccion(double pred) {
			this.prediccion = pred;
		}



		@Override
		public int compareTo(Recomendacion o) {
			return (int) (this.prediccion - o.prediccion);
		}
		
	}
	
	public class AuxHeap implements Comparable<AuxHeap>
	{
		private String nombrePelicula;
		private double valor;
		private Date agno;
		public Date getAgno() {
			return agno;
		}



		public void setAgno(Date date) {
			this.agno = date;
		}



		public int getVotos() {
			return votos;
		}



		public void setVotos(int votos) {
			this.votos = votos;
		}



		public double getPromedioanualvotos() {
			return promedioanualvotos;
		}



		public void setPromedioanualvotos(double d) {
			this.promedioanualvotos = d;
		}



		private int votos;
		private double promedioanualvotos;
		
		
		public String getNombrePelicula() {
			return nombrePelicula;
		}



		public void setNombrePelicula(String nombrePelicula) {
			this.nombrePelicula = nombrePelicula;
		}



		public double getValor() {
			return valor;
		}



		public void setValor(double d) {
			this.valor = d;
		}



		@Override
		public int compareTo(AuxHeap o) {
			return (int) (this.getValor() - o.getValor());
		}
		
	}
	public void setCr(ClienteReq a){
		cr=a;
	}
	@Override
	public ISistemaRecomendacionPeliculas crearSR() {
		
		return null;
	}
	
	public  SistemaRecomendacionPeliculas() {

	}

	@Override
	public boolean cargarPeliculasSR(String rutaPeliculas) {
JSONParser parser = new JSONParser();
		
		try {
			 
            Object obj = parser.parse(rutaPeliculas);
 
            JSONArray jsonObject = (JSONArray) obj;
            
            Iterator<JSONObject> iter = jsonObject.iterator();
            while(iter.hasNext())
            {
            	JSONObject sw = iter.next(); 
            	String id = (String) sw.get("movieId");
            	JSONObject data = (JSONObject)sw.get("imdbData");
            	String nombre = (String) data.get("Title");
            	String fechaLanzamiento = (String) data.get("Released");
            	String generos = (String) data.get("Genre");
            	String votosTotales = (String) data.get("imdbVotes");
            	//de donde saco el promedio de votos anual?
            	String ratingIMDB = (String) data.get("imdbRating");	
            	
            	//agregar VOPelicula
            	VOPelicula niu = new VOPelicula();
            	String[] fecha = fechaLanzamiento.split(" ");
            	String[] genres = generos.split(",");
            	ListaEncadenada<VOGeneroPelicula> ag = new ListaEncadenada<VOGeneroPelicula>();
            	for(String actual : genres)
            	{
            		VOGeneroPelicula agregar = new VOGeneroPelicula();
            		agregar.setNombre(actual);
            		ag.agregarElementoFinal(agregar);
            	}
            	niu.setFechaLanzamineto(new Date(Integer.parseInt(fecha[2]), Integer.parseInt(fecha[1]), Integer.parseInt(fecha[0])));
            	niu.setGenerosAsociados(ag);
            	niu.setNombre(nombre);
            	niu.setPromedioAnualVotos(0);//TODO como se saca?
            	niu.setRatingIMBD(Integer.parseInt(ratingIMDB));
            	niu.setVotostotales(Integer.parseInt(votosTotales));
            	niu.setId(Integer.parseInt(id));
            	misPeliculas.agregarElementoFinal(niu);
            }
            return true;
 
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
	}

	@Override
	public boolean cargarRatingsSR(String rutaRaitings) {
		try {
			CsvReader reader = new CsvReader("\\proyecto_2_201710_sec_2_team_16\\Data\\newRatings-req4.csv");
			reader.readHeaders();
			while(reader.readRecord())
			{
				String id = reader.get("movie");
				String usuario = reader.get("user");
				String rating = reader.get("rating");
				String esterror = reader.get("estimated error");
				String timestamp = reader.get("timestamp");
				
				VOUsuarioPelicula agregar = new VOUsuarioPelicula();
				
				VOPelicula pel = findMovie(Integer.parseInt(id));
				String titulo = pel.getNombre();			
				
				agregar.setErrorRating(Double.parseDouble(esterror));
				agregar.setIdUsuario(Integer.parseInt(usuario));
				agregar.setNombrepelicula(titulo);
				agregar.setRatingSistema(0);
				agregar.setRatingUsuario(Double.parseDouble(rating));
				agregar.setTimestamp(Long.parseLong(timestamp));
				agregar.setTags(darTags(Integer.parseInt(usuario), Integer.parseInt(id)));
				
				ratingspelis.agregarElementoFinal(agregar);
			}
			reader.close();
			return true;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		
	}
	

	@Override
	public boolean cargarTagsSR(String rutaTags) {
		try {
			CsvReader reader = new CsvReader(rutaTags);
			reader.readHeaders();
			while(reader.readRecord())
			{
				String id = reader.get("movieId");
				String usuario = reader.get("userId");
				String tag = reader.get("tag");
				String timestamp = reader.get("timestamp");
				
				//agragar tags a la lista
				VOTag t = new VOTag();
				t.setContenido(tag+";"+id+";"+usuario+";"+timestamp);
				
				tags.agregarElementoFinal(t);
			}
			reader.close();
			return true;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public int sizeMoviesSR() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int sizeUsersSR() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int sizeTagsSR() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void registrarSolicitudRecomendacion(Integer idUsuario, String ruta) {
		if(ruta.equals(""))
		{
			
			
			//armo la solicitud, que es lo que se guardara en el heap
			Solicitud agregar = new Solicitud();
			agregar.setEsporid(true);
			agregar.setIdusuario(idUsuario);
			int contador1 = 0;
			int contador2 = 0;
			int i = 0;
			while(findRatingsByUser(idUsuario).darElementoT(i) != null)
			{
				i++;
				contador1++;
				contador2 += findRatingsByUser(idUsuario).darElementoT(i).getTags().darNumeroElementos();
			}
			agregar.setNumeroratings(contador1);
			agregar.setNumerotags(contador2); 
			
			Date primerRating = new Date(darPrimerRating(idUsuario));
			Date primerTag = new Date (darPrimerTag(idUsuario));
			int cmp = primerRating.compareTo(primerTag);
			if(cmp <= 0)
			{
				agregar.setPrimeraRevision(primerRating);
			}
			else
			{
				agregar.setPrimeraRevision(primerTag);
			}
			//agregamos la solicitud
			heap.insert(agregar);
			
		}
		else if(idUsuario == 0 )
		{
			// opciÃ³n por JSON:
			
			
			//RECUERDE: Utilice los Strings previamente declarados y adaptelos al formato que usted maneja en su proyecto.
			JSONParser parser = new JSONParser();
			 
	        try {
	 
	            Object obj = parser.parse(ruta);
	 
	            JSONObject jsonObject = (JSONObject) obj;
	 
	            JSONObject name = (JSONObject) jsonObject.get("request");
	            String author = (String) name.get("user_name");
	            JSONArray companyList = (JSONArray) name.get("ratings");

	            Iterator<JSONObject> iterator = companyList.iterator();
	            ListaEncadenada<VOUsuarioPelicula> AUX = new ListaEncadenada<VOUsuarioPelicula>();
	            Solicitud agregar = new Solicitud();
	            while (iterator.hasNext()) {
	                JSONObject actual = iterator.next(); 
	                String movieid = (String) actual.get("item_id");
	                String rate = (String) actual.get("rating");
	                
	                //crear la solicitud
	                agregar.setEsporid(false);
	                agregar.setIdusuario(Integer.parseInt(author));
	                VOUsuarioPelicula aux = new VOUsuarioPelicula();
	                aux.setNombrepelicula(findMovie(Integer.parseInt(movieid)).getNombre());
	                aux.setRatingUsuario(Integer.parseInt(rate));
	                AUX.agregarElementoFinal(aux);
	            }
	            heap.insert(agregar);
	 
	        } catch (Exception e) {
	            e.printStackTrace();
	        }
		}
		
	}

	@Override
	public void generarRespuestasRecomendaciones() {
		//sacar las 10 
				ArrayList<ListaEncadenada<Recomendacion>> todas = new ArrayList<ListaEncadenada<Recomendacion>>();
				for(int i = 0; i < 9; i ++)
				{
					ListaEncadenada<Recomendacion> recomendadas = new ListaEncadenada<Recomendacion>();
					Solicitud actual = heap.get();
					ListaEncadenada<VOUsuarioPelicula> dados = findRatingsByUser(actual.getIdusuario());
//						VOPelicula peli = findMovie(actual2.getNombrepelicula());
//						double rating = actual2.getRatingUsuario(); //ratings de todas las peliculas del usuario de la solicitud
						//sacar la prediccion de cada rating de una pelicula con todas las peliculas, imprimir las 10 mas grandes
						Heap<Recomendacion> predicciones = new Heap<Recomendacion>();//llave es el nombre de la pelicula, valor la prediccion
						for(int y = 0; y < misPeliculas.darNumeroElementos(); y++)
						{
							VOPelicula evaluar = misPeliculas.darElementoT(y);
							double pred = pred(dados,evaluar.getNombre());
							Recomendacion rec = new Recomendacion();
							rec.setNombre(evaluar);
							rec.setPrediccion(pred);
							rec.setUsuarioId(Integer.toString(actual.getIdusuario()));
							predicciones.insert(rec);
						}
						
						//saco las 5 mayores predicciones y las monto
						for(int x = 0; x <4; x++)
						{
							recomendadas.agregarElementoFinal(predicciones.get());
						}
						//agrego al arraylist total de solicitudes
						todas.add(recomendadas);
						
					}
				//para escribir en un json, cada solicitud sera un json diferente
				JSONArray obj = new JSONArray();
				for(int i = 0; i < todas.size(); i++)
				{
					JSONObject sw = new JSONObject();
					sw.put("user id", todas.get(i).darElementoT(0).getUsuarioId());
					JSONArray recomendaciones = new JSONArray();
					for(int j = 0; j < todas.get(i).darNumeroElementos();j++)
					{
						recomendaciones.add(todas.get(i).darElementoT(j).getNombre());
					}
					sw.put("recomendadas",recomendaciones);
					obj.add(sw);
				}
				
				Date now = new Date();
				String ano = Integer.toString(now.getYear());
				String mes = Integer.toString(now.getMonth());
				String dia = Integer.toString(now.getDay());
				String hora = Integer.toString(now.getHours());
				String minuto = Integer.toString(now.getMinutes());
				String segundo = Integer.toString(now.getSeconds());
				FileWriter file;
				try {
					file = new FileWriter("\\proyecto_2_201710_sec_2_team_16\\Predicciones\\recomendaciones_"+ano+"-"+mes+"-"+dia+"_"+hora+"_"+minuto+"_"+segundo);
					file.write(obj.toJSONString());
					file.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		
	}

	@Override
	public ILista<VOPelicula> peliculasGenero(VOGeneroPelicula genero, Date fechaInicial, Date fechaFinal) {
		//TODO: retornar un listado ordenado por fecha de todas las peliÌ�culas de un geÌ�nero particular dentro de un rango de fechas dado
				//RECUERDE: diÌ�a /mes /anÌƒo inicial â€“ diÌ�a /mes/ anÌƒo final, usando el mismo formato de fecha que aparece en los datos de las peliÌ�culas
				//crea la tabla de hash ordenada por generos y contiene como dato arboles balanceados ordenados por la fecha de lanzamiento que contienen como dato una pelicula
				EncadenamientoSeparadoTH<String,ArbolRojoNegro<Date,VOPelicula>> arbol = new EncadenamientoSeparadoTH<>(gposibles.darNumeroElementos());
				for(int i = 0; i < gposibles.darNumeroElementos(); i++)
				{
					ArbolRojoNegro<Date, VOPelicula> aux = new ArbolRojoNegro<Date,VOPelicula>();
					for(int j = 0; j < gposibles.darElementoT(i).getPeliculas().darNumeroElementos();i++)
					{
						aux.put(gposibles.darElementoT(i).getPeliculas().darElementoT(j).getFechaLanzamineto(), gposibles.darElementoT(i).getPeliculas().darElementoT(j));
					}
					arbol.insert(gposibles.darElementoT(i).getGenero().getNombre(), aux);
				}
				ListaEncadenada<VOPelicula> resp = new ListaEncadenada<VOPelicula>();
				//TODO
				return  resp;
				
	}

	@Override
	public void agregarRatingConError(int idUsuario, int idPelicula, Double rating) {
		this.AddRating(idUsuario, idPelicula, rating);
		
	}

	@Override
	public ILista<VOUsuarioPelicula> informacionInteraccionUsuario(int idUsuario) {
				ListaEncadenada<VOUsuarioPelicula> resp = new ListaEncadenada<VOUsuarioPelicula>();
				ListaEncadenada<VOUsuarioPelicula> ratings = findRatingsByUser(idUsuario);
				for(int i = 0; i < ratings.darNumeroElementos(); i++)
				{
					VOUsuarioPelicula agregar = new VOUsuarioPelicula();
					VOUsuarioPelicula actual = ratings.darElementoT(i);
					agregar.setNombrepelicula(actual.getNombrepelicula());
					agregar.setRatingUsuario((actual.getRatingUsuario()));
					agregar.setRatingSistema(0); //calcularlo
					agregar.setErrorRating(Math.abs(actual.getRatingSistema()-actual.getRatingUsuario()));
					agregar.setTags(actual.getTags());
					resp.agregarElementoFinal(agregar);
				}
				return resp;
	}

	@Override
	public void clasificarUsuariosPorSegmento() {
		this.clasificarporseg();
		
	}

	@Override
	public void ordenarPeliculasPorAnho() {
		this.ordenarppanho();
		
	}

	@Override
	public VOReporteSegmento generarReporteSegmento(String segmento) {
		return this.genRepSeg(segmento);
	}

	@Override
	public ILista<VOPelicula> peliculasGeneroPorFechaLanzamiento(VOGeneroPelicula genero, Date fechaInicial,
			Date fechaFinal) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ILista<VOPelicula> peliculasMayorPrioridad(int n) {
		Heap<AuxHeap> h = new Heap<AuxHeap>();
		ListaEncadenada<VOPelicula> resp = new ListaEncadenada<VOPelicula>();
		for(int i = 0; i < misPeliculas.darNumeroElementos();i++)
		{
			VOPelicula actual = misPeliculas.darElementoT(i);
			AuxHeap agregar = new AuxHeap();
			agregar.setAgno(actual.getFechaLanzamineto());
			agregar.setNombrePelicula(actual.getNombre());
			agregar.setPromedioanualvotos(actual.getPromedioAnualVotos());
			agregar.setValor(actual.getPromedioAnualVotos() * actual.getRatingIMBD());
			h.insert(agregar);
		}
		
		for(int i = 0; i < n; i++)
		{
			VOPelicula agregar = new VOPelicula();
			AuxHeap actual = h.get();
			agregar.setFechaLanzamineto(actual.getAgno());
			agregar.setNombre(actual.getNombrePelicula());
			agregar.setPromedioAnualVotos(actual.getPromedioanualvotos());
			agregar.setVotostotales(actual.getVotos());
			resp.agregarElementoFinal(agregar);
		}
		return resp;
	}

	@Override
	public ILista<VOPelicula> consultarPeliculasFiltros(Integer anho, String pais, VOGeneroPelicula genero) {
		// TODO Auto-generated method stub
		return null;
	}
	
	/**
	 * debo primero ver que no hayan generos repetidos
	 */
	public void cargarGenerosConPeliculas ()
	{
		for(int i = 0; i < misPeliculas.darNumeroElementos(); i++)
		{
			for(int j = 0; j < misPeliculas.darElementoT(i).getGenerosAsociados().darNumeroElementos();j++)
			{
				if(!contieneGenero(misPeliculas.darElementoT(i).getGenerosAsociados().darElementoT(j)))
				{
					VOGenero gen = new VOGenero();
					gen.setGenero(misPeliculas.darElementoT(i).getGenerosAsociados().darElementoT(j));
					ListaEncadenada<VOPelicula> pelis = new ListaEncadenada<VOPelicula>();
					pelis.agregarElementoFinal(misPeliculas.darElementoT(i));
					gen.setPeliculas(pelis);
					gposibles.agregarElementoFinal(gen);
				}
				else
				{
					boolean encontrado = false;
					for(int x = 0; x < gposibles.darNumeroElementos(); x++)
					{
						if(gposibles.darElementoT(x).getGenero().equals(misPeliculas.darElementoT(i).getGenerosAsociados().darElemento(j)) && !encontrado)
						{
							gposibles.darElementoT(x).getPeliculas().agregarElementoFinal(misPeliculas.darElementoT(i));
						}
					}
				}
			}
		}
	}

	
	public void AddRating(int usuario, int pelicula, Double rating){
		VOUsuarioPelicula agregar = new VOUsuarioPelicula();
		
		VOPelicula pel = findMovie(pelicula);
		String titulo = pel.getNombre();			
		
		agregar.setErrorRating(pred(findRatingsByUser(usuario), findMovie(pelicula).getNombre()));
		agregar.setIdUsuario(usuario);
		agregar.setNombrepelicula(titulo);
		agregar.setRatingSistema(0);
		agregar.setRatingUsuario(rating);
		agregar.setTimestamp(new Date().getTime());
		agregar.setTags(darTags(usuario, pelicula));
		
		ratingspelis.agregarElementoFinal(agregar);
	}

	/**
	 * 
	 */	
	
	/**
	 * 
	 */
	private VOPelicula findMovie(int id)
	{
		for(int i = 0; i < misPeliculas.darNumeroElementos(); i++)
		{
			if(misPeliculas.darElementoT(i).getId() == id)
			{
				return misPeliculas.darElementoT(i);
			}
		}
		return null; //no existe la pelicula
	}
	
	/**
	 * 
	 */
	private VOPelicula findMovie(String nombre)
	{
		for(int i = 0; i < misPeliculas.darNumeroElementos(); i++)
		{
			if(misPeliculas.darElementoT(i).getNombre() == nombre)
			{
				return misPeliculas.darElementoT(i);
			}
		}
		return null; //no existe la peliucla
	}
	
	/**
	 * 
	 */
	private ListaEncadenada<VOTag> darTags(int id,int idpeli)
	{
		ListaEncadenada<VOTag> resp = new ListaEncadenada<VOTag>();
		for(int i = 0; i < tags.darNumeroElementos();i++)
		{
			String[] actual = tags.darElementoT(i).getContenido().split(";");
			
			if(Integer.parseInt(actual[2]) == id && Integer.parseInt(actual[1]) == idpeli)
			{
				resp.agregarElementoFinal(tags.darElementoT(i));
			}
		}
		return resp;
	}
	
	private ListaEncadenada<VOUsuarioPelicula> findRatingsByUser(int id)
	{
		ListaEncadenada<VOUsuarioPelicula> resp = new ListaEncadenada<VOUsuarioPelicula>();
		for(int i = 0; i < ratingspelis.darNumeroElementos(); i++)
		{
			if(ratingspelis.darElementoT(i).getIdUsuario() == id)
			{
				resp.agregarElementoFinal(ratingspelis.darElementoT(i));
			}
		}
		return resp;
	}
	
	private ListaEncadenada<VOUsuarioPelicula> findRatingsByMovie(String nombre)
	{
		ListaEncadenada<VOUsuarioPelicula> resp = new ListaEncadenada<VOUsuarioPelicula>();
		for(int i = 0; i < ratingspelis.darNumeroElementos(); i++)
		{
			if(ratingspelis.darElementoT(i).getNombrepelicula() == nombre)
			{
				resp.agregarElementoFinal(ratingspelis.darElementoT(i));
			}
		}
		return resp;
	}
	
	private Long darPrimerTag(int id)
	{
		Long menor = (long) 0;
		for(int i = 0; i < tags.darNumeroElementos();i++)
		{
			String[] actual = tags.darElementoT(i).getContenido().split(";");
			
			if(Integer.parseInt(actual[2]) == id && menor > Long.parseLong(actual[3]) )
			{
				menor = Long.parseLong(actual[3]);
			}
		}
		return menor;
	}
	
	private Long darPrimerRating(int id)
	{
		Long menor = (long) 0;
		for(int i = 0; i < ratingspelis.darNumeroElementos();i++)
		{
			VOUsuarioPelicula actual = ratingspelis.darElementoT(i);
			
			if(actual.getIdUsuario() == id && menor > actual.getTimestamp()) 
			{
				menor = actual.getTimestamp();
			}
		}
		return menor;
	}
	
	private double sim(String nombrep1, String nombrep2) //que tan parecidas son dos peliculas
	{
		//debo sacar los ratings de ambas peliculas dados por un mismo usuario
		ListaEncadenada<VOUsuarioPelicula> ratingsp1 = findRatingsByMovie(nombrep1);
		ListaEncadenada<VOUsuarioPelicula> ratingsp2 = findRatingsByMovie(nombrep2);
		//sacar ratings de un mismo usuario e irlos multiplicando
		int numerador = 0;
		//los ratings a multiplicar deben tener el mismo usuario
		for(int i = 0; i<ratingsp1.darNumeroElementos(); i++)
		{
			int iduser = ratingsp1.darElementoT(i).getIdUsuario(); //id del usuario a evaluar
			VOUsuarioPelicula pelicula= ratingsp1.darElementoT(i); //primera pelicula
			for(int x = 0; x<ratingsp2.darNumeroElementos(); x++)
			{
				VOUsuarioPelicula actual2 = ratingsp2.darElementoT(x); //segunda pelicula
				if(actual2.getIdUsuario() == iduser) 
				{
					numerador += actual2.getRatingUsuario() * pelicula.getRatingUsuario();
				}
			}
		}
		//sacar la norma de ambos ratings comunes
		double norma1 = 0;
		for(int i = 0; i < ratingsp1.darNumeroElementos(); i++)
		{
			VOUsuarioPelicula pelicula = ratingsp1.darElementoT(i);
			norma1 += pelicula.getRatingUsuario() * pelicula.getRatingUsuario();
		}
		norma1 = Math.sqrt(norma1);
		double norma2 = 0;
		for(int i = 0; i < ratingsp2.darNumeroElementos(); i++)
		{
			VOUsuarioPelicula pelicula = ratingsp2.darElementoT(i);
			norma2 += pelicula.getRatingUsuario() * pelicula.getRatingUsuario();
		}
		norma2 = Math.sqrt(norma2);
		
		return numerador/(norma1*norma2);
	}
	
	private double pred(ListaEncadenada<VOUsuarioPelicula> ratingsdados, String nombrepeli)
	{
		int numerador = 0;
		//para todos los ratings de un usuario, sacar la similitud con nombrepeli y multiplicarlo con el rating de la pelicula evaluada
		for(int i = 0; i< ratingsdados.darNumeroElementos(); i++)
		{
			VOUsuarioPelicula actual = ratingsdados.darElementoT(i);
			double sim = sim(actual.getNombrepelicula(), nombrepeli) * actual.getRatingUsuario();
			numerador += sim;
			
		}
		
		int denominador = 0;
		for(int i = 0; i < ratingsdados.darNumeroElementos();i++)
		{
			VOUsuarioPelicula actual = ratingsdados.darElementoT(i);
			double sim = sim(actual.getNombrepelicula(), nombrepeli);
			denominador += sim;
		}
		return numerador /denominador;
	}
	/**
	 * 
	 */
	private boolean contieneGenero(VOGeneroPelicula voGeneroPelicula)
	{
		boolean resp = false;
		for(int i = 0; i < gposibles.darNumeroElementos(); i++)
		{
			if(gposibles.darElementoT(i).getGenero().getNombre().equals(voGeneroPelicula.getNombre()))
			{
				resp = true;
			}
		}
		return resp;
	}
	
	public void clasificarporseg() {
		// TODO Auto-generated method stub
		
	}

	public ArbolBinario<VOPelicula> ordenarppanho() {
		ArbolBinario<VOPelicula> a=new ArbolBinario<>();
		for (VOPelicula e:misPeliculas){
			a.insert(e);
		}
		return a;
	}

	public VOReporteSegmento genRepSeg(String segmento) {
		VOReporteSegmento a=new VOReporteSegmento();
		return null;
		//TODO FIX
	}
	

	public VOGeneroPelicula findGen(String a){
		for (VOGenero b:gposibles){
			if (b.getGenero().getNombre().equals(a))
				return b.getGenero();
		}
		return null;
	}

	public VOGenero findGen(String a, Boolean b){
		for (VOGenero d:gposibles){
			if (d.getGenero().getNombre().equals(a))
				return d;
		}
		return null;
	}
	public ILista<VOPelicula> pelGen(VOGeneroPelicula genero, Date fechaInicial, Date fechaFinal) {
		ListaEncadenada<VOPelicula> ans=new ListaEncadenada<>();
		VOGenero a = findGen(genero.getNombre(),true);
		for (VOPelicula g:a.getPeliculas()){
			if (g.getFechaLanzamineto().compareTo(fechaInicial)>=0&&g.getFechaLanzamineto().compareTo(fechaFinal)<=0){
				ans.agregarElementoFinal(g);
			}
		}
		return ans;
	}
}
