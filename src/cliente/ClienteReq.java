package cliente;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Date;
import java.util.InputMismatchException;
import java.util.Iterator;
import java.util.Scanner;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.csvreader.CsvReader;

import API.ISistemaRecomendacionPeliculas;
import estructuras.ArbolBinario;
import estructuras.ArbolRojoNegro;
import estructuras.EncadenamientoSeparadoTH;
import estructuras.Heap;
import estructuras.ILista;
import estructuras.ListaEncadenada;
import estructuras.ListaEncadenada.NodoSencillo;
import sun.awt.image.PixelConverter.ArgbPre;
import vos.VOGenero;
import vos.VOGeneroPelicula;
import vos.VOPelicula;
import vos.VOReporteSegmento;
import vos.VOTag;
import vos.VOUsuarioPelicula;


public class ClienteReq {

	BufferedWriter escritor;
	Scanner lector;
	
	
	
	private SistemaRecomendacionPeliculas srp;
	
	
	/**
	 * 
	 */
	public class Solicitud implements Comparable<Solicitud>
	{
		private Date primeraRevision;
		public Date getPrimeraRevision() {
			return primeraRevision;
		}



		private int idusuario; //aqui guardo el id de quein hizo la solicitud
		public int getIdusuario() {
			return idusuario;
		}



		private boolean esporid;
		public boolean isEsporid() {
			return esporid;
		}



		private int numeroratings;
		public int getNumeroratings() {
			return numeroratings;
		}



		private int numerotags;
		public int getNumerotags() {
			return numerotags;
		}



		private ListaEncadenada<VOUsuarioPelicula> aux;
		
		
		
		public void setPrimeraRevision(Date primeraRevision) {
			this.primeraRevision = primeraRevision;
		}



		public void setIdusuario(int idusuario) {
			this.idusuario = idusuario;
		}



		public void setEsporid(boolean esporid) {
			this.esporid = esporid;
		}



		public void setNumeroratings(int numeroratings) {
			this.numeroratings = numeroratings;
		}



		public void setNumerotags(int numerotags) {
			this.numerotags = numerotags;
		}



		@Override
		public int compareTo(Solicitud o) {
			if(this.esporid == false && o.esporid != false )
			{
				return -1;
			}
			else if(o.esporid == false && this.esporid != false)
			{
				return 1;
			}
			else
			{
				if(this.primeraRevision.compareTo(o.primeraRevision) != 0)
				{
					return this.primeraRevision.compareTo(o.primeraRevision);
				}
				else
				{
					if(this.numeroratings > o.numeroratings)
					{
						return 1;
					}
					else if(this.numeroratings < o.numeroratings)
					{
						return -1;
					}
					else
					{
						if(this.numerotags > o.numerotags)
						{
							return 1;
						}
						else if(this.numerotags < o.numerotags)
						{
							return -1;
						}
					}
				}
			}
		return (Integer) null; //esto querria decir que no todos los casos se estan considerando
	}
		



		public ListaEncadenada<VOUsuarioPelicula> getAux() {
			return aux;
		}



		public void setAux(ListaEncadenada<VOUsuarioPelicula> aux) {
			this.aux = aux;
		}
	}
	
	public class Recomendacion implements Comparable<Recomendacion>
	{
		private VOPelicula pelicula;
		private double prediccion;
		private String usuarioId;
		
		
		
		public String getUsuarioId() {
			return usuarioId;
		}



		public void setUsuarioId(String string) {
			this.usuarioId = string;
		}



		public VOPelicula getNombre() {
			return pelicula;
		}



		public void setNombre(VOPelicula nombre) {
			this.pelicula = nombre;
		}



		public double getPrediccion() {
			return prediccion;
		}



		public void setPrediccion(double pred) {
			this.prediccion = pred;
		}



		@Override
		public int compareTo(Recomendacion o) {
			return (int) (this.prediccion - o.prediccion);
		}
		
	}
	
	
public boolean cargarPeliculasSR(String rutaPeliculas)
{
	return srp.cargarPeliculasSR(rutaPeliculas);
}

public boolean cargarRatingsSR(String rutaRatings)
{
	return srp.cargarRatingsSR(rutaRatings);
}

public boolean cargarTags(String rutaTags)
{
	return srp.cargarTagsSR(rutaTags);
}
	




/**
 * 
 * @param escritor
 * @param lector
 */
	public ClienteReq(BufferedWriter escritor, Scanner lector) {
		this.escritor = escritor;
		this.lector = lector;
		srp = new SistemaRecomendacionPeliculas();
		cargarPeliculasSR("\\data\\ml-latest-small\\movies.csv");
		cargarRatingsSR("\\data\\ml-latest-small\\ratings.csv");
	}

	public void pruebas() {
		int opcion = -1;

		//TODO: Inicializar objetos 
		

		while (opcion != 0) {
			try {
				escritor.write("---------------Pruebas Proyecto ---------------\n");
				escritor.write("Requerimientos:\n");
				escritor.write("1: Registrar solicitud de recomendaciÃ³n a un usuario. (R1) \n");
				escritor.write("12: Registrar solicitud de recomendaciÃ³n mediante archivo .JSON (R1) \n");
				escritor.write("2: Generar archivo de respuesta a solicitud de usuarios. (R2) \n");
				escritor.write("3: Generar listado ordenado por fecha de peliculas en un gÃ©nero especÃ­fico dentro de un rango de fechas. (R3) \n");
				escritor.write("4: AnÌƒadir un nuevo rating a una peliÌ�cula y guardar el error de prediccioÌ�n (R4) \n");
				escritor.write("5: Generar un informe con toda la informacioÌ�n que se tiene sobre un usuario y su interaccioÌ�n con las peliÌ�culas. (R5) \n");
				escritor.write("6: Se desea tener una lista con los identificadores de usuarios clasificados en un segmento dado.(R6) \n");
				escritor.write("7: Ordenar las peliÌ�culas del cataÌ�logo en un aÌ�rbol binario balanceado y ordenarlo por el anÌƒo de la peliÌ�cula. (R7) \n");
				escritor.write("8: Realizar un reporte para describir los segmentos de los usuarios.(R8) \n");
				escritor.write("9: Buscar peliÌ�culas de un geÌ�nero cuya fecha de lanzamiento se encuentre en un periodo de tiempo dado por diÌ�a /mes /anÌƒo inicial y diÌ�a /mes /anÌƒo final (R9) \n");
				escritor.write("10: Obtener las N peliÌ�culas de mayor prioridad en su orden. (R10) \n");
				escritor.write("11: Generar lista de peliculas de acuerdo a los 3 criterios ingresados por el usuario (R11) \n");
				escritor.write("0: Volver\n");
				escritor.write("------------------------------------------------\n");
				escritor.flush();
				opcion = lector.nextInt();

				switch(opcion) {
				case 1: r1(); break;
				case 2: r2(); break;
				case 3: r3(); break;
				case 4: r4(); break;
				case 5: r5(); break;
				case 6: r6(); break;
				case 7: r7(); break;
				case 8: r8(); break;
				case 9: r9(); break;
				case 10: r10(); break;
				case 11: r11(); break;
				}
			}
			catch (IOException ioe) {
				ioe.printStackTrace();
			}
			catch (NumberFormatException nfe) {
				try {
					escritor.write("No ingreso el periodo de tiempo correctamente\n");
					escritor.write("Ingrese cualquier letra y enter para continuar\n");
					escritor.flush();
					lector.nextLine();
					lector.nextLine();
				}
				catch (IOException ioe) {
					ioe.printStackTrace();
				}
			}
			catch (InputMismatchException ime) {
				try {
					escritor.write("No ingreso un numeral\n");
					escritor.write("Ingrese cualquier letra y enter para continuar\n");
					escritor.flush();
					lector.nextLine();
					lector.nextLine();
				}
				catch (IOException ioe) {
					ioe.printStackTrace();
				}
			}
		}
	}

	private void r1() throws IOException{ 

		//: Registrar solicitud de recomendaciÃ³n a un usuario. Recuerde que hay dos tipos de solicitud
		//RECUERDE: Usted debe manejar si el parametro es por usuario con ID o la ruta del JSON.
		
		escritor.write("escoger si es por JSON o id de usuario");
		escritor.write("1 para id, 2 para JSON");
		escritor.flush();
		
		srp.setCr(this);
		int res = lector.nextInt();
		if(res == 1)
		{
			escritor.write("Ingrese el id del usuario");
			escritor.flush();
			int idUsuario = lector.nextInt();
			srp.registrarSolicitudRecomendacion(idUsuario, ""); //no sera necesario tener en cuenta la ruta
		}
		else if(res == 2)
		{
			escritor.write("Ingresar la ruta del JSON");
			escritor.flush();
			String ruta = lector.nextLine();
			srp.registrarSolicitudRecomendacion(0, ruta); //al leer el json se maneja bien el idUsuario 
		}
		


		long tiempo = System.nanoTime();

		//TODO: Llamar mÃ©todo registrarSolicitudRecomendacion(Integer idUsuario, String ruta) del API
		
		
		
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}


	private void r2() throws IOException{ 

		srp.generarRespuestasRecomendaciones();
		

		long tiempo = System.nanoTime();
		//TODO: Llamar mÃ©todo generarRespuestasRecomendaciones() del API
		//Se espera como resultado: ruta del archivo generado. 

		String ruta = "carpeta de recomendaciones";
		tiempo = System.nanoTime() - tiempo;
		escritor.write("El archivo se creÃ³ con la ruta: " + ruta + "\n");
		escritor.write("\n");

		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}

	private void r3() throws NumberFormatException, IOException
	{
		
		escritor.write("Ingrese la fecha inicial: \n");
		escritor.flush();
		String fechaInicial = lector.next();
		System.out.println(fechaInicial);
	
		escritor.write("Ingrese la fecha final: \n");
		escritor.flush();
		String fechaFinal = lector.next();
		System.out.println(fechaFinal);
	
		escritor.write("Ingrese el genero a buscar: \n");
		escritor.flush();
		String genero = lector.next();
		System.out.println(genero);
	
		//RECUERDE: Utilice los Strings previamente declarados y adaptelos al formato de fechas y genero que usted maneja en su proyecto.
		long tiempo = System.nanoTime();
	
		//TODO: Llamar mÃ©todo peliculasGeneroPorFechas(VOGeneroPelicula genero, Date fechaInicial, Date fechaFinal) del API
		//Se espera como resultado: lista VOPeliculas
		VOGeneroPelicula a = new VOGeneroPelicula();
		a.setNombre(genero);
		srp.peliculasGenero(a,  new Date(Long.parseLong(fechaInicial)), new Date(Long.parseLong(fechaFinal)));
		
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}


	

	private void r4() throws IOException
	{
		//TODO: Se desea que los usuarios puedan seguir anÌƒadiendo ratings a las peliÌ�culas; cada vez que un usuario anÌƒade un rating se compara el rating dado con la prediccioÌ�n dada por el sistema.
		//RECUERDE: El valor absoluto de la diferencia entre el rating otorgado por el usuario y la prediccioÌ�n del sistema es el error.
		//RECUERDE: Cada vez que se anÌƒade un rating se debe tener registro de cuaÌ�nto fue el error para el rating otorgado.

	
		escritor.write("Ingrese el ID del Usuario: \n");
		escritor.flush();
		String idUsuario = lector.next();
		System.out.println(idUsuario);

		escritor.write("Ingrese el ID de la pelicula: \n");
		escritor.flush();
		String idPelicula = lector.next();
		System.out.println(idPelicula);

		escritor.write("Ingrese el nuevo Rating \n");
		escritor.flush();
		String rating = lector.next();
		System.out.println(rating);

		//RECUERDE: Utilice los Strings previamente declarados y adaptelos al formato que usted maneja en su proyecto.
		long tiempo = System.nanoTime();

		//TODO: Llamar mÃ©todo agregarRatingConError(int idUsuario, int idPelicula, Double rating) del API
		SistemaRecomendacionPeliculas i=new SistemaRecomendacionPeliculas();
		i.setCr(this);
		i.agregarRatingConError(Integer.parseInt(idUsuario), Integer.parseInt(idPelicula), Double.parseDouble(rating));
		

		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}

	private void r5() throws IOException{

		//TODO: generar un informe con toda la informacioÌ�n que se tiene sobre un usuario y su interaccioÌ�n con las peliÌ�culas.
		// RECUERDE: informacion a retornar en la lista: nombre Pelicula, rating otorgado por el usuario, rating calculado por el sistema,
		//error del rating, tags que el usuario otorgÃ³ a la pelÃ­cula. 
		
		//RECUERDE: El error se calcula como el valor absoluto de la diferencia entre los ratings. 
		escritor.write("Ingrese el ID del usuario: \n");
		escritor.flush();
		String usuarioID = lector.next();
		srp.informacionInteraccionUsuario(Integer.parseInt(usuarioID));
		
		
		
		long tiempo = System.nanoTime();

		//TODO: Llamar mÃ©todo informacionInteraccionUsuario(int idUsuario) del API
		SistemaRecomendacionPeliculas i=new SistemaRecomendacionPeliculas();
		i.setCr(this);
		i.informacionInteraccionUsuario(Integer.parseInt(usuarioID));
		
		
		//Se espera como resultado: lista VOUsuarioPelicula 

		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}

	private void r6() throws IOException{

		//TODO:Se desea tener una lista con los identificadores de usuarios clasificados en un segmento dado.
		//RECUERDE: 4 tipos o segmentos diferentes: Inconformes, Conformes, Neutrales y No Clasificado.


		long tiempo = System.nanoTime();

		//TODO: Llamar mÃ©todo clasificarUsuariosPorSegmento() del API
		SistemaRecomendacionPeliculas i=new SistemaRecomendacionPeliculas();
		i.setCr(this);
		i.clasificarUsuariosPorSegmento();		
		
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}

	private void r7() throws IOException{

		//TODO:ordenar las peliÌ�culas del cataÌ�logo en un aÌ�rbol binario balanceado y ordenado por el anÌƒo de la peliÌ�cula.

		//RECUERDE:Dentro de cada nodo del aÌ�rbol, se tiene una lista, en la que en cada nodo se tiene una peliÌ�cula y 
		//una tabla de hash con los usuarios que han asignado un tag especiÌ�fico a dicha peliÌ�cula.



		long tiempo = System.nanoTime();

		//TODO: Llamar mÃ©todo ordernarPelicuaPorAnho() del API
		SistemaRecomendacionPeliculas i=new SistemaRecomendacionPeliculas();
		i.setCr(this);
		i.ordenarPeliculasPorAnho();

		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}

	private void r8() throws IOException{

		//TODO:  Dado un segmento se quiere saber lo siguiente: el error promedio, sus 5 geÌ�neros con maÌ�s cantidad de ratings y sus 5 geÌ�neros con mejor rating promedio.
		//RECUERDE: Error promedio = suma de errores sobre sus ratings dividido la cantidad de ratings con error asociado. 

		escritor.write("Ingrese el segmento: \n");
		escritor.flush();
		String segmento = lector.next();
		System.out.println(segmento);

		//RECUERDE: Utilice los Strings previamente declarados y adaptelos al formato que usted maneja en su proyecto.

		long tiempo = System.nanoTime();

		//TODO: Llamar mÃ©todo generarReporteSegmento(String segmento) del API 
		//Se espera como resultado: VOReporteSegmento 
		SistemaRecomendacionPeliculas i=new SistemaRecomendacionPeliculas();
		i.setCr(this);
		i.generarReporteSegmento(segmento);
		
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}

	private void r9() throws NumberFormatException, IOException
	{
		//TODO: Buscar peliÌ�culas de un geÌ�nero cuya fecha de lanzamiento se encuentre en un periodo de tiempo dado
		//RECUERDE: diÌ�a /mes /anÌƒo inicial â€“ diÌ�a /mes/ anÌƒo final, usando el mismo formato de fecha que aparece en los datos de las peliÌ�culas
		//RECUERDE: Tenga en cuenta que para las peliÌ�culas, su mes de lanzamiento (campo Released) estaÌ� dado por las 3 iniciales en ingleÌ�s (Jan, Feb, Mar, Apr, ...). 

		escritor.write("Ingrese la fecha inicial: \n");
		escritor.flush();
		String fechaInicial = lector.next();
		System.out.println(fechaInicial);

		escritor.write("Ingrese la fecha final: \n");
		escritor.flush();
		String fechaFinal = lector.next();
		System.out.println(fechaFinal);

		escritor.write("Ingrese el genero a buscar: \n");
		escritor.flush();
		String genero = lector.next();
		System.out.println(genero);

		//RECUERDE: Utilice los Strings previamente declarados y adaptelos al formato de fechas y genero que usted maneja en su proyecto.
		long tiempo = System.nanoTime();

		//TODO: Llamar mÃ©todo peliculasGeneroPorFechaLanzamiento(VOGeneroPelicula genero, Date fechaInicial, Date fechaFinal) del API 
		//Se espera como resultado: lista VOPelicula		
		SistemaRecomendacionPeliculas i=new SistemaRecomendacionPeliculas();
		i.setCr(this);
		String[] fechaI = fechaInicial.split(" ");
		String[] fechaF = fechaFinal.split(" ");
		i.peliculasGenero(srp.findGen(genero), new Date(Integer.parseInt(fechaI[2]), Integer.parseInt(fechaI[1]), Integer.parseInt(fechaI[0])), new Date(Integer.parseInt(fechaF[2]), Integer.parseInt(fechaF[1]), Integer.parseInt(fechaF[0])));
		
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}


	private void r10() throws IOException{

		//TODO: A partir del Heap se quiere obtener las N peliÌ�culas de mayor prioridad en su orden, donde el valor N es dado por el usuario.
		//RECUERDE:  Para cada peliÌ�cula resultante, hay que mostrar su tiÌ�tulo, anÌƒo, votos totales, promedio anual votos y prioridad de clasificacioÌ�n. 

		escritor.write("Ingrese el N a buscar en el SR.\n");
		escritor.flush();
		int peliculas = lector.nextInt();
		System.out.println(peliculas);
		
		//RECUERDE: Utilice la variable previamente declarada y adaptela al formato que usted maneja en su proyecto.
		ListaEncadenada<VOPelicula> resp = (ListaEncadenada<VOPelicula>) srp.peliculasMayorPrioridad(peliculas);
		for(int i = 0; i < peliculas; i++)
		{
			VOPelicula actual = resp.darElementoT(i);
			System.out.println(actual.getNombre() + Integer.toString(actual.getVotostotales())+ Double.toString(actual.getPromedioAnualVotos()));
		}
		
		long tiempo = System.nanoTime();

		//TODO: Llamar mÃ©todo peliculasMayorPrioridad(int n) del API 
		//Se espera como resultado: lista VOPelicula		
		
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	}

	private void r11() throws IOException{

		//TODO: Las peliÌ�culas resultantes deben obtenerse en el siguiente orden: Inicialmente ordenadas por anÌƒo. 
		//Para las peliÌ�culas del mismo anÌƒo, deben ordenarse por paiÌ�s (alfabeÌ�ticamente). 
		//Para peliÌ�culas del mismo paiÌ�s deben ordenarse por geÌ�nero. Para peliÌ�culas del mismo geÌ�nero deben ordenarse por rating IMBD. 
		//Tenga en cuenta que una peliÌ�cula puede tener asociado muÌ�ltiples paiÌ�ses y muÌ�ltiples geÌ�neros.

		//RECUERDE: los criterios pueden ser dados en cualquier orden para el filtro. 
		escritor.write("Ingrese criterio1: \n");
		escritor.flush();
		String criterio1 = lector.next();
		System.out.println(criterio1);

		escritor.write("Ingrese criterio2: \n");
		escritor.flush();
		String criterio2 = lector.next();
		System.out.println(criterio2);

		escritor.write("Ingrese el criterio3: \n");
		escritor.flush();
		String criterio3 = lector.next();
		System.out.println(criterio3);

		//RECUERDE: Utilice las variables previamente declaradas y adaptelas al formato que usted maneja en su proyecto.

		long tiempo = System.nanoTime();

		//TODO: Llamar mÃ©todo consultarPeliculasFiltros (Integer anho, String pais, VOGeneroPelicula genero) del API 
		//Se espera como resultado: lista VOPelicula		
		
		tiempo = System.nanoTime() - tiempo;
		escritor.write("Duracion: " + tiempo + " nanosegundos\n");
		escritor.write("Ingrese cualquier letra y Enter para continuar\n");
		escritor.flush();
		lector.next();
	
	}

}
	
	